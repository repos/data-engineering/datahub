# DataHub
This repository comprises the build pipeline for the container images for DataHub that the Wikimedia Foundation runs in production.

There are 8 container images built and published by this pipeline:

* datahub-gms
* datahub-frontend
* datahub-mce-consumer
* datahub-mae-consumer
* datahub-upgrade
* datahub-mysql-setup
* datahub-elasticsearch-setup
* datahub-kafka-setup

Each push to a feature branch executes the full set of build pipelines.
The unit testing suite is maintained by the upstream authors and is run as part of the build process, which is why we do not have a test stage.

After a merge to the main branch is the full set of publish pipelines is executed.
